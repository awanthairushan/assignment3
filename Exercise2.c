#include <stdio.h>
int main(){
    int num,sum=0;
    printf("Input a Number : ");
    scanf("%d",&num);
    if(num<=1){
        printf("\n%d is not a prime number\n",num);
        return 0;
    }
    for(int i=2; i<num; i++){
        sum+=num%i;
        if(num%i==0){
            printf("\n%d is not a prime number\n",num);
            return 0;
        }
    }
    printf("\n%d is a prime number\n",num);
    return 0;
}

